package com.example.primerejercicio

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView

class MainActivity : AppCompatActivity() {

    private lateinit var welcomeMessageTextView: TextView
    private lateinit var welcomeSubtitleTextView: TextView
    private lateinit var changerTextButton: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        welcomeMessageTextView=findViewById(R.id.textView2)
        //welcomeSubtitleTextView=findViewById(R.id.textView)
        //changerTextButton.setOnClickListener{(changeMessageAndSubtitle())}
        changerTextButton=findViewById(R.id.button)


    }

    private fun changeMessageAndSubtitle(){

        welcomeMessageTextView.text= "Hello Universe"
        welcomeSubtitleTextView.text="Hey Therel"

    }

}
